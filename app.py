from flask import Flask,jsonify
from utils import Utils
from apscheduler.schedulers.background import BackgroundScheduler          #for cron scheduling
import atexit



app = Flask(__name__)

# cron
sched = BackgroundScheduler({'apscheduler.timezone': 'Asia/Calcutta'})
sched.start()
atexit.register(lambda: sched.shutdown())


@sched.scheduled_job(trigger='cron', hour = 0,timezone="UTC") 
# @sched.scheduled_job(trigger='interval', minutes = 3) 
def changePwd():
    status,data = utils.changePwd()


if __name__ == '__main__' :

    utils = Utils()
    
    app.run(debug=False,host='0.0.0.0', port='5000',use_reloader=True)
