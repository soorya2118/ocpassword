import configparser
import ssl
import requests
import paramiko
import subprocess
import random
import string
import socket
import sys,os
import datetime
import smtplib

from pymongo import MongoClient, errors
from bson.objectid import ObjectId
from pprint import pprint
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText



class Utils:
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('.env')

        connectionString = self.config['vpn_db']['string']
        try:
            conn = MongoClient(connectionString)
            print("connected to  db......")

        except Exception as e:
            print("Not connected to  db....", e)
        
        self.profile  = conn.vpn_db['profiles']

    """
    For generating random 6 digit numeric password
    """
    def generatePwd(self):
        letters = string.digits
        passwd = ''.join(random.choice(letters) for i in range(6))
        return True,passwd

    """
    For sending email with password details as attachment
    """

    def sendEmail(self,email_1,email_2,email_3,subject):
        print("inside email sending")
        from_address = self.config['email']['email_id']
        login        = from_address
        to_address   = [email_1,email_2,email_3]
        password     = self.config['email']['password']
        priority     ='2'
        try:
            msg = MIMEMultipart('alternative')

            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.ehlo()  
            server.starttls()   
            server.login(login,password)  
            msg['Subject'] = subject
            msg['From'] = from_address
            msg['To'] = ', '.join(to_address)
            body = 'VPN Password updation Information'
            msg.attach(MIMEText(body))

            with open('Vpn_password_updates.txt', 'r') as file:
                attachment = MIMEText(file.read())
                file.close()
                attachment.add_header(
                                        'Content-Disposition', 
                                        'attachment', 
                                        filename="Vpn_password_updates.txt")           
            msg.attach(attachment)
            server.sendmail(from_address, to_address, msg.as_string())
            server.quit() 
            return True,"Mail sent"
        except Exception as e:
            print(e)
            return False,str(e)

    """
    For changing the open connection password on servers and db.
    1. Get the details of VPN apps from db
    2. Connect to each domain using paramiko via SSH
    3. Execute the command for changing the ocpasswd
    4. Enter and re enter new ocpasswd
    5. Close connection
    6. Update the new ocpasswd in db
    7. Update the file Vpn_password_updates.txt with new passwd information
    8. After all server passwords are reset send the file to email
    """
    def changePwd(self):
        try:
            profiles = list(self.profile.find(
                    
                    {
                        "status"          : True 
                    },
                    {
                        "_id"        : 0,
                        "domain"     : 1
                    }
                ))
            # pprint(profiles)
            if len(profiles) == 0:
                return False,"No profiles found in DB"
            print(len(profiles))
            
            # print(profiles)
            profiles_minimized = []
            for item in profiles:
                if item not in profiles_minimized:
                    profiles_minimized.append(item)

            # print(profiles_minimized)
            print(len(profiles_minimized))
            domain_dict = {}

            for prof in profiles_minimized:
            
                domain = (prof['domain']).split(":")[0]
                status,new_passwd = self.generatePwd()
                
                if status == False:
                    return False,"Error in password generation"

                ssh_username = self.config['creds']['username']
                ssh_password = self.config['creds']['password']
                
                ssh_client=paramiko.SSHClient()
                ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

                try:
                    ssh_client.connect(
                                        hostname = domain,
                                        username = ssh_username,
                                        password = ssh_password,
                                        timeout  = 60
                                        )
                    
                    print("SSH connection success")
                

                except socket.timeout as e:

                    print("Connection timed out ",e)
                    return False,"Socket timeout"

                except Exception as e:
                    print("SSH connection failed ",e)
                    return False,str(e)
                
                stdin, stdout, stderr = ssh_client.exec_command(
                                "sudo ocpasswd -c /etc/ocserv/ocpasswd todouser",
                                # timeout=10, 
                                # get_pty=True
                                )

                new_passwd_to_enter = f"{new_passwd}"+'\n'
                stdin.write(new_passwd_to_enter)
                stdin.write(new_passwd_to_enter)
                stdin.flush()

                print(stdout.read().decode())
                err = stderr.read().decode()
                # print("error....",err)
                if err:
                    print(err)
                ssh_client.close()

                try:
                    result = self.profile.update_many({
                        "domain"       : prof['domain']
                    },
                    {
                        "$set": {
                            "password" : new_passwd
                        }
                    })
                    
                except Exception as e:
                    print("Mongo db udation failed ",e)
                    # print(type(e))
                    return False,"Mongo db udation failed"
                # print(result)
                # print(result.acknowledged)
                if result.acknowledged == False:
                    return False,"DB updation failed"

                prof['pwd_updated'] = True
                domain_dict[domain] = new_passwd

                try:
                    current_time = datetime.datetime.now()
                    hours = 5.5
                    hours_added = datetime.timedelta(hours = hours)
                    local_time = current_time + hours_added
                    password_updates = f"""\n\n
                    Updated Time : {current_time} UTC,\n
                    Updated Time : {local_time} Local,\n
                    Domain   : {prof['domain']},\n
                    Password : {new_passwd}
                    """
                    f = open("Vpn_password_updates.txt", "a")
                    f.write(password_updates)
                    f.close()
                except Exception as e:
                    print("Error in file writing",e)
                    return False,str(e)
            

            try:
                email_1 = self.config['email']['to_mail_1']
                email_2 = self.config['email']['to_mail_2']
                email_3 = self.config['email']['to_mail_3']

                status,message = self.sendEmail(
                    email_1,email_2,email_3,"Password updates")
                if status == False:
                    return False,"Error in sending mail"
            
            except Exception as e:
                print("Error in sendig mail ",e)
                return False,str(e)

            return True,"OK"
        except socket.timeout:
            # print("socket time out error")
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return False,"Socket timeout error"

        except Exception as e:
            # print("error : ",e)
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            return False,str(e)

